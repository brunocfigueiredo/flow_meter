
/*
 * File:   main.c
 * Author: bmalbusca
 *
 * Created on 31 de Janeiro de 2015, 04:00
 */

#include <p30F6012A.h>
#include <libpic30.h>

#include <stdio.h>
#include <math.h>
#include <stdbool.h>

/*********************************************************************
 * Configuration bits
 *********************************************************************/

/*
Clock switching and Fail-safe clock disabled
External clock multiplier = 4/2 = 2
*/
_FOSC(CSW_FSCM_OFF & HS2_PLL4);

/*
Watch-dog timer off --- enabling it in software instead
*/
_FWDT(WDT_OFF);

/*
Brown Out voltage = 2.7V & BOR circuit disabled
Power On Reset timer = 16ms
/MCLR enabled
*/
_FBORPOR(BORV_27 & PBOR_OFF & PWRT_16 & MCLR_EN);

/*
Code protection off
*/
_FGS(CODE_PROT_OFF);

/*
Select comm channel
*/
_FICD(PGD);


/* Timer value at Fcy that corresponds to 10ms */
#define M_SEC FCY*0.001



/**********************************************************************
 * Global Variables
***********************************************************************/


unsigned int frequency;
unsigned int period;
unsigned int counter;


unsigned int latchA1 = 0
unsigned int latchA2 = 0
unsigned int lat = PORTBbits.RB0; // some port.



/**********************************************************************
 * Name:    port_config
 * Args:    -
 * Return:  -
 * Desc:    -
 **********************************************************************/


void port_config(void){


ADPCFGbits.PCFG0 = 1; /* Initialize pin as digital */

TRISBbits.TRISB0 = 1; /* Set as digital input */

return;

}

/**********************************************************************
 * Name:    timer1_config
 * Args:    -
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void timer1_config(void) {

	T1CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T1CONbits.TGATE = 0;		/* Gated mode off                        */
	T1CONbits.TCKPS = 0;		/* prescale 1:1                          */
	T1CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR1 = 0;					/* clears the timer register             */
	PR1 = M_SEC;				/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC0bits.T1IP = 2;			/* Timer 1 Interrupt Priority 0-7        */
	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T1IE = 1;			/* Timer 1 Interrupt Enable              */


	T1CONbits.TON = 1;			/* starts the timer                      */
	return;
}


/**********************************************************************
 * Assign Timer 1 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt (void){

	static int i=0;

	if(i>1000){		/* execute only once every x(ms) */
		parsebuffer();
		i=0;
	}
	i++;


	IFS0bits.T1IF = 0;		/* clears interruption flag */
	return;
}


/**********************************************************************
 * Name:  InitTimer2 
 * Args:
 * Return:
 * Desc:  IC1 timer
 **********************************************************************/


void InitTimer2(void)
{


    T2CON = 0; // Clear Timer 1 configuration
    
    T2CONbits.TSIDL = 0;
    T2CONbits.TON = 0;      /* Timer2 is used for generating PWM frequency */
    T2CONbits.TCS = 0;

    T2CONbits.TGATE = 0;
    T2CONbits.TCKPS = 3; /* Set timer 2 prescaler (0=1:1, 1=1:8, 2=1:64, 3=1:256)*/
    
    IPC1bits.T2IP = 4; /* prior4 */
    
    IEC0bits.T2IE = 1;
    
    PR2 = 65535;
    TMR2 = 0 ;
    T2CONbits.TON = 1;      /* TIMER 2 ON */
    
  
}


/**********************************************************************
 * Name:    IC1_config
 * Args:    -
 * Return:  -
 * Desc:    input capture control register.
 **********************************************************************/

void IC1_config(void){

IC1CONbits.ICM= 0b00;         /* Disable Input Capture 1 module */
IC1CONbits.ICTMR= 1;         /* Select Timer2 as the IC1 Time base */
IC1CONbits.ICI= 0b00;         /* Interrupt on every capture event */
IC1CONbits.ICM= 0b010;         /* Generate capture event on every falling edge */
							/* Enable Capture Interrupt And Timer2 */
IPC0bits.IC1IP = 1;         /* Setup IC1 interrupt priority level */
IFS0bits.IC1IF = 0;         /* Clear IC1 Interrupt Status Flag */
IEC0bits.IC1IE = 1;         /* Enable IC1 interrupt */

return;

}

/**********************************************************************
 * Assign IC1 interruption
 **********************************************************************/

void __attribute__( ( interrupt , auto_psv ) ) _IC1Interrupt( void ){

if (counter == 2){
		period = TMR2;
		TMR2 = 0;
		counter = 0;
	}	


IFS0bits.IC1IF = 0;	
}



/**********************************************************************
 * Name:   count
 * Args:   lat, counter
 * Return: 
 * Desc: detects tachometer signal
 **********************************************************************/



void count(unsigned int lat, unsigned int counter) {
    

  if(lat==1){
    latchA1 = 1;
  }
  if(lat==0){
    latchA2= 1;
  }
  if(latchA1== 1 and latchA2== 1){
    counter++; //increment counter 
   
    //reset latches
    latchA1= 0;
    latchA2= 0;
  }

   
}


/**********************************************************************
 * Name:   Initfun
 * Args:  
 * Return: 
 * Desc:   
 **********************************************************************/

void InitFun(void) {
    


    port_config();

    IC1_config();
    
	InitTimer2();
     
    timer1_config();

	CAN2_config();

    
    return;
}



/**********************************************************************
 * Main
 **********************************************************************/

int main() {
    
  

InitFun();


while(1) {


	count(lat, counter);
    
    frequency = 1/period ;

}


return 0;

}
